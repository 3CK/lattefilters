<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

use Nette\Utils\Html;

trait Svg
{
	private string $SVG_DIR = WWW_DIR . '/images/inlineSvgs/';

	private string $SPRITES_DIR = WWW_DIR . '/images/sprites/';

	private array $sprites = [];


	public function sprite(string $name, string $classes = null): Html
	{
		return $this->useSprite($name, $classes);
	}


	public function useSprite(string $name, string $classes = null, string $directory = null): Html
	{
		if (!array_key_exists($name, $this->sprites)) {
			$dir = $this->SPRITES_DIR . ($directory ? $directory . '/' : null);
			$path = $dir . $name . '.svg';
			$content = file_get_contents($path);
			$svg = Html::el()->addHtml($content);
			$this->sprites[$name] = $svg;
		}

		return Html::el()->addHtml('<svg ' . ($classes ? 'class="' . $classes . '"' : '') . '><use xlink:href="#' . $name . '"></use></svg>');
	}


	public function insertSprites(): Html
	{
		$tags = Html::el();

		foreach ($this->sprites as $sprite) {
			$tags->addHtml($sprite);
		}

		return $tags;
	}


	public function preloadSprite(string $name): Html
	{
		if (!array_key_exists($name, self::$sprites)) {
			$path = $this->SPRITES_DIR . $name . '.svg';
			$content = file_get_contents($path);
			$svg = Html::el()->addHtml($content);
			$this->sprites[$name] = $svg;
		}

		return Html::el();
	}


	public function svg(string $name, bool $uniqueIds = false): Html
	{
		$path = $this->SVG_DIR . $name . '.svg';
		$content = file_get_contents($path);

		if ($uniqueIds) {
			$hash = bin2hex(random_bytes(5));
			$content = preg_replace('/id="([0-9a-zA-Z_-]+)"/', 'id="$1-' . $hash . '"', $content);
			$content = preg_replace('/\((#[0-9a-zA-Z_-]+)\)/', '($1-' . $hash . ')', $content);
		}

		$element = Html::el()->addHtml($content);
		return $element;
	}
}
