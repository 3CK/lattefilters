<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

trait Phone
{
	public function phone(string $number = null): string
	{
		if (!$number) {
			return '';
		}

		$number = trim($number);
		$number = (int) preg_replace('/^(\+[0-9]{3})/', '', $number, 1);
		return number_format($number, 0, '.', ' ');
	}


	public function telHref(string $number = null): string
	{
		if (!$number) {
			return '';
		}

		$number = str_replace(' ', '', $number);
		$number = preg_replace('/^(\+420)/', '', $number, 1);
		return 'tel:' . $number;
	}
}
