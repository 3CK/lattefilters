<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

trait Money
{
	public function money(int $number, string $currency = 'Kč', bool $useFree = false)
	{
		if ($number == 0 && $useFree) {
			return 'zdarma';
		}

		$number = number_format($number, 0, ',', ' ');
		$result = $number . ' ' . $currency;

		return $result;
	}
}
