<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

use Nette\Utils\Html;

trait Script
{
	private array $scripts = [];


	public function script($file): string
	{
		$this->scripts[] = $file;
		return '';
	}


	public function insertScripts(): Html
	{
		$tags = Html::el();

		$this->scripts = array_unique($this->scripts);

		foreach ($this->scripts as $script) {
			$file = '/js/' . $script . '.js';
			if (file_exists(WWW_DIR . $file)) {
				$version = date('U', filemtime(WWW_DIR . $file));
				$path = $this->basePath . $file . '?v=' . $version;
				$tags->addHtml(Html::el('script', ['src' => $path]));
			} else {
				throw new \Exception('Script ' . $file . ' not found!');
			}
		}

		return $tags;
	}
}
