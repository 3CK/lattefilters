<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

use Nette\Utils\Html;

trait Style
{
	private array $styles = [];


	public function css(string $file): string
	{
		return $this->style($file);
	}


	public function style(string $file): string
	{
		$this->styles[] = $file;
		return '';
	}


	public function insertStyles(): Html
	{
		$tags = Html::el();

		$this->styles = array_unique($this->styles);

		foreach ($this->styles as $style) {
			$attributes = [
				'rel' => 'stylesheet',
				'type' => 'text/css',
			];

			$file = '/css/' . $style . '.css';

			if (file_exists(WWW_DIR . $file)) {
				$version = date('U', filemtime(WWW_DIR . $file));
				$path = $this->basePath . $file . '?v=' . $version;
				$attributes['href'] = $path;
				$tags->addHtml(Html::el('link', $attributes));
			} else {
				throw new \Exception('Style ' . $file . ' not found.');
			}
		}

		return $tags;
	}
}
