<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

use Nette\Utils\Html;
use Trick\ImageManager\ImageSet;

trait Picture
{
	private string $imagesDomain = '';


	private function installPictureFilter(array $settings): void
	{
		if (array_key_exists('imagesDomain', $settings)) {
			$this->imagesDomain = $settings['imagesDomain'];
		}
	}


	public function picture(ImageSet $imageSet, string $alt = null, string $imgVersion = null, string $classes = null): Html
	{
		$tag = Html::el('picture');
		$alt = $alt ? strip_tags($alt) : null;

		if ($imgVersion) {
			$images = $imageSet->getVariant($imgVersion);
		} else {
			$images = $imageSet->getAllImages();
		}

		$lastImage = end($images);

		foreach ($images as $image) {
			if ($this->imagesDomain) {
				$src = $this->imagesDomain . $image->getPath();
			} else {
				$src = $this->basePath . $image->getPath();
			}

			if ($image !== $lastImage) {
				$tag->addHtml(Html::el('source', [
					'srcset' => $src,
					'type' => $image->getType()->getMime()
				]));
			} else {
				$tag->addHtml(Html::el('img', [
					'src' => $src,
					'alt' => $alt,
					'title' => $alt,
					'class' => $classes,
				]));
			}
		}

		return $tag;
	}
}
