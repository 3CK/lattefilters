<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;


trait Date
{
	private array $DAYS = [
		'cs' => [
			'full' => [
				'1' => ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
				'2' => ['Neděle', 'Pondělí', 'Úterý', 'Středy', 'Čtvrtku', 'Pátku', 'Soboty'],
				'3' => ['Neděli', 'Pondělí', 'Úterý', 'Středě', 'Čtvrtku', 'Pátku', 'Sobotě'],
				'4' => ['Neděli', 'Pondělí', 'Úterý', 'Středu', 'Čtvrtek', 'Pátek', 'Sobotu'],
				'5' => ['Neděle', 'Pondělí', 'Úterý', 'Středo', 'Čtvrtku', 'Pátku', 'Soboto'],
				'6' => ['Neděli', 'Pondělí', 'Úterý', 'Středě', 'Čtvrtku', 'Pátku', 'Sobotě'],
				'7' => ['Nedělí', 'Pondělím', 'Úterým', 'Středou', 'Čtvrtkem', 'Pátkem', 'Sobotou'],
			],
			'short' => ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
		],
		'en' => [
			'short' => ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
			'full' => ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
		],
	];

	private array $MONTHS = [
		'cs' => [
			1 => [ 1=> 'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
			3 => [ 1=> 'ledna', 'února', 'března', 'dubna', 'května', 'června', 'července', 'srpna', 'září', 'října', 'listopadu', 'prosince']
		]
	];


	public function monthName(int $month, string $lang = 'cs', int $casus = 1): string
	{
		return $this->MONTHS[$lang][$casus][$month];
	}


	public function dayName($day, string $lang = 'cs', int $casus = 1): string
	{
		if (!array_key_exists($lang, $this->DAYS)) {
			throw new \LogicException('dayName macro: No dictionary for language ' . $lang . '.');
		} elseif ($lang !== 'en' && !array_key_exists($casus, $this->DAYS[$lang]['full'])) {
			throw new \LogicException('dayName macro: No dictionary for case ' . $casus . '.');
		} elseif ((int) $day < 0 || (int) $day > 6) {
			throw new \LogicException('dayName macro: Day has to be a number between 0 and 6. Do you use "w" date formatter?');
		}

		if ($lang !== 'en') {
			return $this->DAYS[$lang]['full'][$casus][$day];
		} else {
			return $this->DAYS[$lang]['full'][$day];
		}
	}


	public function dayNameShort($day, $lang = 'cs'): string
	{
		if (!array_key_exists($lang, $this->DAYS)) {
			throw new \LogicException('dayName macro: No dictionary for language ' . $lang . '.');
		} elseif ($day < 0 || $day > 6) {
			throw new \LogicException('dayName macro: Day has to be a number between 0 and 6. Do you use "l" date formatter?');
		}

		return $this->DAYS[$lang]['short'][$day];
	}


	public function czechDate($date): string
	{
		return $date->format('j. n. Y');
	}


	public function czechDateTime($date): string
	{
		return $date->format('j. n. Y H:i');
	}


	public function dateTimeText(\DateTime $date = null): string
	{
		if (!$date) {
			return '';
		}

		$time = $date->getTimestamp();

		$diff = time() - $time;
		$sec = $diff;
		$min = round($diff / 60);
		$hrs = round($diff / 3600);
		$days = round($diff / 86400);
		$weeks = round($diff / 604800);
		$mnths = round($diff / 2600640);
		$yrs = round($diff / 31207680);

		if ($sec <= 60) {
			if ($sec < 1) {
				return 'právě teď';
			} elseif ($sec == 1) {
				return 'před vteřinou';
			} else {
				return 'před ' . $sec . ' vteřinami';
			}
		} else if ($min <= 60) {
			if ($min <= 1) {
				return 'před minutou';
			} else {
				return 'před ' . $min . ' minutami';
			}
		} else if ($hrs <= 24) {
			if ($hrs == 1) {
				return 'před hodinou';
			} else {
				return 'před ' . $hrs . ' hodinami';
			}
		} else if ($days <= 7) {
			if ($days == 1) {
				return 'včera ' . $date->format('H:i');
			} else {
				return 'před ' . $days . ' dny';
			}
		} else if ($weeks <= 4.3) {
			if ($weeks == 1) {
				return 'před týdnem';
			} else {
				return 'před ' . $weeks . ' týdny';
			}
		} else if ($mnths <= 12) {
			if ($mnths == 1) {
				return 'před měsícem';
			} else {
				return 'před ' . $mnths . ' měsíci';
			}
		} else {
			if ($yrs == 1) {
				return 'před rokem';
			} else {
				return 'před ' . $yrs . ' lety';
			}
		}
	}
}
