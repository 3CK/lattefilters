<?php
declare(strict_types=1);

namespace Trick\LatteFilters\Filter;

trait Text
{
	public function camel(string $text): string
	{
		return preg_replace_callback(
			'/-([^-])/',
			function (array $m) {
				return ucfirst($m[1]);
			},
			$text
		);
	}

	public function decline(int $num, string $one, string $twoToFour, string $more): string
	{
		if ($num == 1) {
			return $one;
		} elseif ($num > 1 && $num < 5) {
			return $twoToFour;
		} else {
			return $more;
		}
	}

}
