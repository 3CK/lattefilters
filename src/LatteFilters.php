<?php
declare(strict_types=1);

namespace Trick\LatteFilters;

use Nette\Http\IRequest;

class LatteFilters
{
	use Filter\Style;
	use Filter\Svg;
	use Filter\Date;
	use Filter\Picture;
	use Filter\Phone;
	use Filter\Money;
	use Filter\Script;
	use Filter\Text;


	public string $basePath;


	public function __construct(array $settings, IRequest $httpRequest)
	{
		$baseUri = rtrim($httpRequest->getUrl()->getBaseUrl(), '/');
		$this->basePath = preg_replace('#https?://[^/]+#A', '', $baseUri);

		$this->installPictureFilter($settings);
	}


	public function load(string $filter): ?callable
	{
		if (method_exists($this, $filter)) {
			return [ $this, $filter ];
		} else {
			return null;
		}
	}
}
